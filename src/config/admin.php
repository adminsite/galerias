<?php

return array(

	'dominios' => array(
		'http://admsite.net',
		'http://www.admsite.net',
		'http://www.myadmin.tk',
		'http://myadmin.tk',
		'http://adminsite.dev'
	),

	'mimeTypes' => array(
		'image/jpeg',
		'image/pjpeg',
		'image/png'
	)

);