<?php

/**
 * Filtro para la comunicacion de la API
 *
 * @return Bool
 */
App::after(function($request, $response)
{
	if (Config::has('adm.cors')) {
		$response->header('Access-Control-Allow-Origin', Config::get('adm.cors'));
	}

	$response->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
	$response->header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, X_FILENAME, Content-Type, Content-Range, Content-Disposition, Content-Description');
});

Route::pattern('path', '[a-z0-9_\/]+');
Route::pattern('img', '[a-z0-9_]+(\.[a-z]{3})$');
Route::get('adm/pics/galerias/secciones/{path}/{img}', '\Adminsite\Galerias\Controllers\ImagenesController@getIndex');

Route::group(array(
	'prefix'    => 'adm/api/v1', 
	'before'    => 'api', 
	'namespace' => '\Adminsite\Galerias\Controllers'), function()
{
	Route::resource('galerias', 'GaleriaController');
	Route::resource('galerias/secciones', 'SeccionController');

	//Imagenes
	Route::controller('galerias/secciones/pic/{path}/{img}', 'ImagenesController');
});