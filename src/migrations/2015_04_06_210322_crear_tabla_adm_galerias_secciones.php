<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAdmGaleriasSecciones extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_galerias_secciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('alias')->unique();
			$table->string('nombre');
			$table->mediumText('descripcion');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_galerias_secciones');
	}

}
