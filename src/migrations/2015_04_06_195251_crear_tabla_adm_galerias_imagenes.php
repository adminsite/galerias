<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAdmGaleriasImagenes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adm_galerias_imagenes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('archivo');
			$table->string('directorio');
			$table->string('titulo');
			$table->mediumText('descripcion');
			$table->morphs('imageable');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adm_galerias_imagenes');
	}

}
