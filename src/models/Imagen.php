<?php

namespace Adminsite\Galerias;

use DB;
use Illuminate\Validation\Validator;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Imagen extends Eloquent
{
	//Tabla
	protected $table = 'adm_galerias_imagenes';

	/**
	 * Reglas para la validacion
	 * 
	 * @var Array
	 */
	public static $reglas = array(
	);

	/**
	 * Instancia Validador
	 * @var Illuminate\Validation\Validators
	 */
	protected $validator;

	public function __construct(array $attributes = array(), Validator $validator = null)
	{
		parent::__construct($attributes);
		$this->validator = $validator ?: \App::make('validator');
	}

	/**
	 * Validates current attributes against reglas
	 * @return Bollean
	 */
	public function validate()
	{
		$v = $this->validator->make($this->attributes, static::$reglas, static::$mensajes);
		if ($v->passes())
		{
			return true;
		}
		$this->setErrors($v->messages());
		return false;
	}

	/**
	 * Set error message bag
	 * 
	 * @var Illuminate\Support\MessageBag
	 */
	protected function setErrors($errors)
	{
		$this->errors = $errors;
	}

	public function imageable()
	{
		return $this->morphTo();
	}

}