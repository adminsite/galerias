<?php

namespace Adminsite\Galerias\Controllers;

use Adminsite\Galerias\Galeria,
	Adminsite\Galerias\Imagen;

use Input,
	Request,
	Response,
	File,
	Config;

class GaleriaController extends \BaseController
{
	private $response = array(
		'error' => false
	);

	private $path = 'adm/galerias/';

	public function __construct()
	{
		//Crear directorio de articulos si no existen
		if (File::isDirectory($this->path) == false) {
			File::makeDirectory($this->path, 0777, true, true);
		}

		$gitignore = 'adm/.gitignore';
		if (File::exists($gitignore) == false) {
			File::put($gitignore, '*');
		}
	}


	public function index()
	{
		if (Config::has('adm.galerias')) {
			$secciones = Config::get('adm.galerias');

			if (is_array($secciones)) {
				foreach ($secciones as $key => $value) 
				{
					if (array_key_exists('alias', $value)) {
					 	$alias = $value['alias'];
					 	if (preg_match('/(\W)/', $alias) == false) {
							$data['secciones'][] = $value;
					 	}
					} else {
						continue;
					}
				}
			}
		}

		$galerias = Galeria::paginate(20);
		$data['galerias'] = $galerias;

		return Response::json($data, 200);
	}


	public function store ()
	{
		try
		{
			$galeria = new Galeria;
			$galeria->nombre      = Input::get('nombre');
			$galeria->descripcion = Input::get('descripcion', '');
			$galeria->ref         = time();

			$galeria->save();

			//Crear directorio para el slider
			$dir = $this->path . 'glr'.$galeria->ref.'/';
			if (File::isDirectory($dir) == false) {
				File::makeDirectory($dir, 0777, true, true);
			}

			if (Input::hasFile('imagenes')) {
				$imagenes = array();

				foreach (Input::file('imagenes') as $archivo) {
					//Generar ID de imagen
					$bytes = openssl_random_pseudo_bytes(4);
					$hex   = bin2hex($bytes);

					$img = new Imagen;
					$img->archivo    = $hex.'.'.strtolower($archivo->getClientOriginalExtension());
					$img->directorio = $dir;

					$upload = $archivo->move($img->directorio, $img->archivo);
					if ($upload) {
						$imagenes[] = $img;
					}
				}

				$galeria->imagenes()->saveMany($imagenes);
			}
		}
		catch(\Exception $e)
		{
			$msg = json_decode($e->getMessage());
			$this->response['error']   = true;
			$this->response['mensaje'] = (is_array($msg) or is_object($msg)) ? $msg : $e->getMessage();
			return Response::json($this->response, '400');
		}
	}


	public function show($id)
	{
		$galeria = Galeria::where('id', $id)->with('imagenes')->first();
		return Response::json($galeria);
	}


	public function update ($id)
	{
		try
		{
			$galeria = Galeria::find($id);
			$galeria->nombre      = Input::get('nombre');
			$galeria->descripcion = Input::get('descripcion', '');

			$galeria->save();

			//Crear directorio para el slider
			$dir = $this->path . 'glr'.$galeria->ref.'/';
			if (File::isDirectory($dir) == false) {
				File::makeDirectory($dir, 0777, true, true);
			}

			if (Input::hasFile('imagenes')) {
				$imagenes = array();

				foreach (Input::file('imagenes') as $archivo) {
					//Generar ID de imagen
					$bytes = openssl_random_pseudo_bytes(4);
					$hex   = bin2hex($bytes);

					$img = new Imagen;
					$img->archivo    = $hex.'.'.strtolower($archivo->getClientOriginalExtension());
					$img->directorio = $dir;

					$upload = $archivo->move($img->directorio, $img->archivo);
					if ($upload) {
						$imagenes[] = $img;
					}
				}

				$galeria->imagenes()->saveMany($imagenes);
			}
		}
		catch(\Exception $e)
		{
			$msg = json_decode($e->getMessage());
			$this->response['error']   = true;
			$this->response['mensaje'] = (is_array($msg) or is_object($msg)) ? $msg : $e->getMessage();
			return Response::json($this->response, '400');
		}
	}


	public function destroy ($id)
	{
	}
}
