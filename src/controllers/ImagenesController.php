<?php

namespace Adminsite\Galerias\Controllers;

use Adminsite\Galerias\Imagen;
// import the Intervention Image Manager Class
use \Intervention\Image\ImageManagerStatic as Image,
	Response, Request, DB, File;

class ImagenesController extends \BaseController
{
	private $response = array(
		'error'=>false
	);

	public function getIndex($path, $img)
	{
		$data = array(
			'filter' => Request::input('f'),
			'path' => public_path().'/adm/galerias/'.$path.'/'.$img
		); 

		if (File::exists($data['path']) == false) {
			$data['path'] = 'http://fakeimg.pl/850x450/00CED1/FFF/?text=imagen+no+encontrada';
		}

		$image = Image::cache(function($image) use ($data)
		{
			$image->make($data['path']);

			switch ($data['filter'])
			{
				case 'xs':
					$image->fit(180, 120);
				break;

				case 'sm':
					$image->fit(260, 200, function ($constraint) {
						$constraint->aspectRatio();
					});
				break;

				case 'lg':
					if (Request::has('w') and Request::has('h')) {
						$image->fit(Request::input('w'), Request::input('h'), function ($constraint) {
							$constraint->upsize();
						});
					} else {
						$image->resize(850, null, function ($constraint) {
							$constraint->aspectRatio();
						});
					}
				break;

				default:
					$image->fit(260, 200, function ($constraint) {
						$constraint->aspectRatio();
					});
				break;
			}
			
			return $image;

		}, 10, true);

		$response = Response::make($image->encode('jpg'));
		$response->header('Content-Type', 'image/jpg');

		return $response;
	}


	public function deleteIndex ($path, $img)
	{
		try
		{
			DB::beginTransaction();

			$imagen = Imagen::where(array('archivo'=>$img, 'directorio'=>$path.'/'));
			$imagen->delete();

			File::delete('adm/galerias/'.$path.'/'.$img);

			DB::commit();
			return Response::json($this->response, '200');
		}
		catch(\Exception $e)
		{
			DB::rollBack();
			$this->response['error']   = true;
			$this->response['mensaje'] = $e->getMessage();
			return Response::json($this->response, '400');
		}

	}

}
