<?php

namespace Adminsite\Galerias\Controllers;

use Adminsite\Galerias\Seccion,
	Adminsite\Galerias\Imagen;

use Input,
	Request,
	Response,
	File,
	Config,
	Str,
	DB,
	Cache;

class SeccionController extends \BaseController
{
	private $response = array(
		'error' => false
	);

	private $filesTypes = array('image/jpg', 'image/jpeg');

	private $path = 'adm/galerias/';

	public function __construct()
	{
		//Crear directorio de galerias si no existen
		if (File::isDirectory($this->path) == false) {
			File::makeDirectory($this->path, 0777, true, true);
		}

		$gitignore = 'adm/.gitignore';
		if (File::exists($gitignore) == false) {
			File::put($gitignore, '*');
		}
	}


	private function getConfig($nombre)
	{
		if (Config::has('adm.galerias')) {
			$secciones = Config::get('adm.galerias');
			$alias     = array_pluck($secciones, 'alias');

			if (in_array($nombre, $alias)) {
				$clave   = array_search($nombre, $alias);
				return current(array_slice($secciones, $clave, 1));			
			}

			return false;
		}

		return false;
	}


	public function index()
	{
	}


	public function store ()
	{
		try
		{
			DB::beginTransaction();
			
			$config   = $this->getConfig(Input::get('alias'));
			$archivos = Input::file('archivos');

			if ($config == false) {
				throw new Exception("No se encuentra la configuracion correcta.", 1);
			}

			//Ingresar nueva seccion en base de datos
			$seccion = Seccion::firstOrNew(array('alias'=>Input::get('alias')));
			$seccion->nombre      = Input::get('nombre');
			$seccion->descripcion = Input::get('descripcion');
			$seccion->save();

			//Contador de archivos a cargar
			$max   = (int) $config['max'];
			$count = count($archivos);
			$size  = ($count > $max) ? $max : $count;

			//Crear directorio para el slider
			$nombre  = Input::get('alias');
			$carpeta = 'secciones/'.$nombre.'/';
			$dir     = $this->path . $carpeta;

			if (File::isDirectory($dir) == false) {
				File::makeDirectory($dir, 0777, true, true);
			}

			/**
			 * Subir Archivos
			 */
			if (Input::hasFile('archivos')) {
				$imagenes = array();

				for ($i=0; $i < $size; $i++)
				{
					if ($archivos[$i]->isValid() == false and in_array($archivos[$i]->getMimeType(), $config['mimes']) == false) {
						continue;
					}

					//Generar ID de imagen
					$bytes = openssl_random_pseudo_bytes(4);
					$hex   = bin2hex($bytes);

					$img = new Imagen;
					$img->archivo     = $hex.'.'.strtolower($archivos[$i]->getClientOriginalExtension());
					$img->directorio  = $carpeta;
					
					$upload = $archivos[$i]->move($dir, $img->archivo);
					if ($upload) {
						$imagenes[] = $img;
					}
				}

				$seccion->imagenes()->saveMany($imagenes);
			}	

			/**
			 * Borrar Cache
			 */
			if (Cache::has('adm.galerias.seccion.'.Input::get('alias'))) {
				Cache::forget('adm.galeria.seccion.'.Input::get('alias'));
			}

			DB::commit();
			return Response::json(array('imagenes'=>$seccion->imagenes()->get()));
		}
		catch(\Exception $e)
		{
			DB::rollBack();
			$msg = json_decode($e->getMessage());
			$this->response['error']   = true;
			$this->response['mensaje'] = (is_array($msg) or is_object($msg)) ? $msg : $e->getMessage();
			return Response::json($this->response, '400');
		}
	}


	public function show($alias)
	{
		$imagenes = null;
		$seccion  = Seccion::where('alias', $alias)->with('imagenes')->first();

		if ($seccion) {
			$imagenes = $seccion->imagenes;
		}

		return Response::json(array('imagenes'=>$imagenes), 200);
	}


	public function update ($id)
	{
	}


	public function destroy ($id)
	{
	}
}
