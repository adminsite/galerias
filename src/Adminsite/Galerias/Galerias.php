<?php 

namespace Adminsite\Galerias;

use Config, Cache, URL;

class Galerias
{
	private $path = 'adm/galerias/secciones/';

	private $galeria = null;

	public function seccion($nombre)
	{
		if (Cache::has('adm.galerias.seccion.'.$nombre)) {
			Cache::forget('adm.galerias.seccion.'.$nombre);
			$this->galeria = Cache::get('adm.galeria.seccion.'.$nombre);
			
			return $this;
		}

		if (Config::has('adm.galerias')) {
			$secciones = Config::get('adm.galerias');
			$alias     = array_pluck($secciones, 'alias');

			if (in_array($nombre, $alias)) {
				$clave   = array_search($nombre, $alias);
				$seccion = current(array_slice($secciones, $clave, 1));

				$galeria = Seccion::where('alias', $seccion['alias'])->with('imagenes')->first();

				if ($galeria) {
					$this->galeria = $galeria->toArray();
					Cache::add('adm.galerias.seccion.'.$nombre, $this->galeria, 2880);				
				}

				return $this;
			}
		}

		return false;
	}

	public function imagenes ()
	{
		$imagenes = array();
		
		if ($this->galeria == null) {
			return $imagenes;
		}

		foreach ($this->galeria['imagenes'] as $key => $img) 
		{
			$imagenes[] = URL::to('adm/pics/galerias/secciones/'.$img['directorio'].$img['archivo']);
		}

		return $imagenes;
	}
}