<?php

namespace Adminsite\Galerias\Facades;

use Illuminate\Support\Facades\Facade;

class Galerias extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'galerias';
	}
}